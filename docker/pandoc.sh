#!/bin/sh

for file in /data/*.md; do
  pandoc -f markdown --standalone -F pandoc-mermaid -t revealjs ${file} -o ${file%.*}.html
done
