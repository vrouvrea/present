#!/bin/bash

# Requires latest git version for PUPPETEER_CFG management. Not in pip install version
git config --global user.name "Vincent Rouvreau" &&  \
git config --global user.email vincent.rouvreau@inria.fr &&  \
git clone https://github.com/timofurrer/pandoc-mermaid-filter.git --depth 1 &&  \
cd pandoc-mermaid-filter &&  \
python -m pip install .

npm install @mermaid-js/mermaid-cli
export PATH="$(pwd)/node_modules/.bin:$PATH"
export MERMAID_BIN="$(pwd)/node_modules/.bin/mmdc"
export PUPPETEER_CFG="$(pwd)/../docker/puppeteer-config.json"
