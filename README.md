# Present

`present` is a combo of `pandoc`, `reveal.js` and `mermaid.js` to help to make beautiful and efficient presentations.

## Docker image

### Run the image

```bash
cd /markdown/presentation/path
docker pull registry.gitlab.inria.fr/vrouvrea/present/ci:main
docker run --rm -u `id -u`:`id -g` -v `pwd`:/data registry.gitlab.inria.fr/vrouvrea/present/ci:main
```

This will transform all `/markdown/presentation/path/*.md` to `/markdown/presentation/path/*.html` as a reveal presentation.


### Build the image

```bash
cd /my/workspace/path
cd present/docker
docker build -t  present:latest .
```

Then run the image (as explained above, with an image as `present:latest`).

## In a conda environment

### Setup

```bash
cd /my/workspace/path
cd present/conda
conda env create -f conda/environment.yml
conda activate present
./post-install.sh
```

### Run

```bash
cd /markdown/presentation/path/
pandoc -f markdown --standalone -F pandoc-mermaid -t revealjs presentation.md -o presentation.html
```
