% Present
% Vincent Rouvreau

---

## Present


![](present.png)

`present` is a combo of `pandoc`, `reveal.js` and `mermaid.js` to help to make beautiful and efficient presentations.

---

## mermaid.js example in markdown

```mermaid
%%{init: { 'theme': 'dark', 'gitGraph': {'showBranches': true, 'showCommitLabel':false }} }%%
gitGraph
    commit
    commit
    branch intro
    branch conclusion
    checkout intro
    commit
    checkout conclusion
    commit
    checkout intro
    commit
    checkout conclusion
    commit
    checkout main
    merge intro
    checkout conclusion
    commit
    checkout main
    merge conclusion
```
---

## Thanks !

---


